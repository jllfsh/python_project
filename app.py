from handler import handler
from jinja2 import Environment, FileSystemLoader


def application(environ, start_response):
    template_env = Environment(loader=FileSystemLoader('templates'))

    template_path, template_vars, headers = handler.get_params(environ)
    template = template_env.get_template(template_path)

    start_response('200 OK', headers)

    return [template.render(template_vars).encode('utf-8')]
