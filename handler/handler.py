import Cookie
import urlparse


def get_params(environ):
    headers = []

    if environ['PATH_INFO'] == '/':
        if environ['REQUEST_METHOD'] == 'POST':
            content_length = int(environ.get('CONTENT_LENGTH'))
            query_string = environ['wsgi.input'].read(content_length)
        else:
            query_string = environ['QUERY_STRING']

        template_vars = {
                    'title': 'Params Table',
                    'query_params': urlparse.parse_qs(query_string)
        }

        template_path = 'base.html'

    elif environ['PATH_INFO'] == '/cookies':
        if 'HTTP_COOKIE' in environ:
            cookies = Cookie.SimpleCookie(environ['HTTP_COOKIE'])
            if 'counter' in cookies:
                cookies['counter'] = int(cookies['counter'].value) + 1
        else:
            cookies = Cookie.SimpleCookie()
            cookies['counter'] = 1
        cookies['counter']['expires'] = 24 * 3600

        headers = [('set-cookie', cookies['counter'].OutputString())]

        template_vars = {
                    'title': 'Cookie Counter',
                    'cookies': cookies['counter'].value
        }

        template_path = 'cookies.html'

    elif environ['PATH_INFO'] == '/form':
        template_vars = {
            'title': 'Form',
        }

        template_path = 'form.html'

    else:
        template_vars = {
            'title': 'Error',
            'message': 'Page not found'
        }

        template_path = 'error.html'

    return [template_path, template_vars, headers]